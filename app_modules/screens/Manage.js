import React, {Component} from 'react';
import {
  Text,
  ScrollView,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';

const {width: WIDTH} = Dimensions.get('window');

class Manage extends Component {
  render() {
    return (
      <ScrollView>
        <View>
          <Text>Cancel the ticket to your ride</Text>
          <View>
            <TouchableOpacity>
              <Text>Back</Text>
            </TouchableOpacity>
            <TouchableOpacity>
              <Text>CANCEL TICKET</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const manageStyle = StyleSheet.create({
  primaryView: {
    flex: 1,
    height: 200,
    width: WIDTH,
    backgroundColor: '#212121',
    padding: 30,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  ticketText: {
    color: 'white',
    fontWeight: '400',
    fontSize: 16,
    fontFamily: 'notoserif',
    bottom: 5,
  },
  keyText: {
    color: '#FF5722',
    fontWeight: '700',
    fontSize: 18,
    fontFamily: 'notoserif',
    bottom: 5,
  },
  secondaryView: {
    flex: 1,
    width: WIDTH,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  body: {
    backgroundColor: '#D7CCC8',
  },
  menuBtn: {
    width: WIDTH - 150,
    padding: 15,
    marginTop: 15,
    marginHorizontal: 25,
    alignItems: 'center',
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    backfaceVisibility: 'hidden',
    backgroundColor: 'white',
  },
  btnText: {
    fontWeight: '600',
    fontSize: 17,
    left: 0,
    right: 10,
    fontFamily: 'notoserif',
    color: '#212121',
  },
});

export default Manage;

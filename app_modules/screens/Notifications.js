import React, {Component} from 'react';
import {
  Text,
  TextInput,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
  View,
  ScrollView,
} from 'react-native';
import {CheckBox} from 'react-native-elements';

const {width: WIDTH} = Dimensions.get('window');

class Notifications extends Component {
  state = {
    checked: false,
  };
  render() {
    return (
      <ScrollView>
        <View style={notificationStyle.primaryView}>
          <Text style={notificationStyle.introText}>
            {' '}
            Get notification when your bus arrives{' '}
          </Text>
          <CheckBox
            center
            title="Get notified via SMS"
            checkedIcon="dot-circle-o"
            uncheckedIcon="circle-o"
            checked={this.state.checked}
          />
          <Text>
            Please enter your phone number to receive notifications via SMS
          </Text>
          <TextInput
            style={notificationStyle.input}
            placeholder={'0700112233'}
            underlineColorAndroid="transparent"
          />
          <TouchableOpacity style={notificationStyle.submitBtn}>
            <Text style={notificationStyle.btnText}>SUBMIT</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const notificationStyle = StyleSheet.create({
  primaryView: {
    flex: 1,
    width: WIDTH,
    padding: 30,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  input: {
    width: WIDTH - 80,
    height: 40,
    borderRadius: 7,
    paddingLeft: 45,
    backgroundColor: '#fff',
    marginTop: 20,
    color: 'black',
    marginHorizontal: 25,
  },
  submitBtn: {
    backgroundColor: '#FF5722',
    width: WIDTH - 200,
    height: 45,
    borderRadius: 0,
    marginTop: 40,
    color: 'white',
    marginHorizontal: 25,
    alignItems: 'center',
    alignSelf: 'center',
    backfaceVisibility: 'hidden',
  },
  btnText: {
    fontWeight: '500',
    fontSize: 17,
    top: 10,
    left: 0,
    right: 10,
    fontFamily: 'notoserif',
    color: '#FFFFFF',
  },
  introText: {
    fontWeight: '700',
    fontSize: 18,
    margin: 20,
    fontFamily: 'notoserif',
  },
});

export default Notifications;

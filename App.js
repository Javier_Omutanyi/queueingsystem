import 'react-native-gesture-handler';
import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  ActivityIndicator,
  StatusBar,
  Text,
} from 'react-native';

// Screen import
import Login from '/home/javier/final_Project/queueingSystem/QueueingSystem/app_modules/screens/Login.js';
import Signup from '/home/javier/final_Project/queueingSystem/QueueingSystem/app_modules/screens/Signup.js';
import Ticket from '/home/javier/final_Project/queueingSystem/QueueingSystem/app_modules/screens/Ticket.js';
import Queue from '/home/javier/final_Project/queueingSystem/QueueingSystem/app_modules/screens/Queue.js';
import Track from '/home/javier/final_Project/queueingSystem/QueueingSystem/app_modules/screens/Track.js';
import Manage from '/home/javier/final_Project/queueingSystem/QueueingSystem/app_modules/screens/Manage.js';
import Notifications from '/home/javier/final_Project/queueingSystem/QueueingSystem/app_modules/screens/Notifications.js';
import Profile from '/home/javier/final_Project/queueingSystem/QueueingSystem/app_modules/screens/Profile.js';
import Records from '/home/javier/final_Project/queueingSystem/QueueingSystem/app_modules/screens/Records.js';

// Navigation Imports
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

// Redux imports
import {Provider} from 'react-redux';
import {
  store,
  persistor,
} from '/home/javier/final_Project/queueingSystem/QueueingSystem/app_modules/Store/store.js';
import {PersistGate} from 'redux-persist/integration/react';

import Icon from 'react-native-vector-icons/Ionicons';

const Stack = createStackNavigator();

export default class App extends Component {
  render() {
    const Loading = () => (
      <View>
        <ActivityIndicator />
        <Text>Initializing...</Text>
      </View>
    );

    return (
      <Provider store={store}>
        <PersistGate loading={<Loading />} persistor={persistor}>
          <NavigationContainer>
            <StatusBar backgroundColor="#5D4037" barStyle="light-content" />
            <Stack.Navigator>
              <Stack.Screen
                name="LOGIN"
                component={Login}
                options={{
                  headerStyle: {
                    backgroundColor: '#5D4037',
                  },
                  headerTitleAlign: 'center',
                  headerTintColor: '#fff',
                  headerTitleStyle: {
                    fontWeight: 'bold',
                  },
                }}
              />
              <Stack.Screen
                name="Signup"
                component={Signup}
                options={{
                  headerStyle: {
                    backgroundColor: '#5D4037',
                  },
                  headerTitleAlign: 'center',
                  headerTintColor: '#fff',
                  headerTitleStyle: {
                    fontWeight: 'bold',
                  },
                }}
              />
              <Stack.Screen
                name="Queue"
                component={Queue}
                options={{
                  headerStyle: {
                    backgroundColor: '#5D4037',
                  },
                  headerLeft: () => (
                    <Icon
                      // eslint-disable-next-line react-native/no-inline-styles
                      style={[{color: 'white', margin: 25}]}
                      size={24}
                      name={'ios-menu'}
                    />
                  ),
                  headerTitleAlign: 'center',
                  headerTintColor: '#fff',
                  headerTitleStyle: {
                    fontWeight: 'bold',
                  },
                }}
              />
              <Stack.Screen
                name="Ticket"
                component={Ticket}
                options={{
                  headerStyle: {
                    backgroundColor: '#5D4037',
                  },
                  headerTitleAlign: 'center',
                  headerTintColor: '#fff',
                  headerTitleStyle: {
                    fontWeight: 'bold',
                  },
                }}
              />
              <Stack.Screen
                name="Track"
                component={Track}
                options={{
                  headerStyle: {
                    backgroundColor: '#5D4037',
                  },
                  headerTitleAlign: 'center',
                  headerTintColor: '#fff',
                  headerTitleStyle: {
                    fontWeight: 'bold',
                  },
                }}
              />
              <Stack.Screen
                name="Manage"
                component={Manage}
                options={{
                  headerStyle: {
                    backgroundColor: '#5D4037',
                  },
                  headerTitleAlign: 'center',
                  headerTintColor: '#fff',
                  headerTitleStyle: {
                    fontWeight: 'bold',
                  },
                }}
              />
              <Stack.Screen
                name="Notifications"
                component={Notifications}
                options={{
                  headerStyle: {
                    backgroundColor: '#5D4037',
                  },
                  headerTitleAlign: 'center',
                  headerTintColor: '#fff',
                  headerTitleStyle: {
                    fontWeight: 'bold',
                  },
                }}
              />
              <Stack.Screen
                name="Profile"
                component={Profile}
                options={{
                  headerStyle: {
                    backgroundColor: '#5D4037',
                  },
                  headerTitleAlign: 'center',
                  headerTintColor: '#fff',
                  headerTitleStyle: {
                    fontWeight: 'bold',
                  },
                }}
              />
              <Stack.Screen
                name="Records"
                component={Records}
                options={{
                  headerStyle: {
                    backgroundColor: '#5D4037',
                  },
                  headerTitleAlign: 'center',
                  headerTintColor: '#fff',
                  headerTitleStyle: {
                    fontWeight: 'bold',
                  },
                }}
              />
            </Stack.Navigator>
          </NavigationContainer>
        </PersistGate>
      </Provider>
    );
  }
}

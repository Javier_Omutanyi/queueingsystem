import React, {Component} from 'react';
import {
  Text,
  Dimensions,
  ScrollView,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  View,
  Alert,
} from 'react-native';

// firebase authentication import
import firebase from '@react-native-firebase/app';

const {width: WIDTH} = Dimensions.get('window');

class Signup extends Component {
  constructor(props) {
    super(props);
    this.unsubscriber = null;
    this.state = {
      isAuthenticated: false,
      username: '',
      // phone: '',
      typedEmail: '',
      typedPassword: '',
      user: null,
    };
  }

  onSignup = () => {
    firebase
      .auth()
      .createUserWithEmailAndPassword(
        this.state.typedEmail,
        this.state.typedPassword,
      )
      .then(() => {
        Alert.alert(
          'Successfull',
          'Your account has been created successfully',
          [
            {
              Text: 'Okay',
              onPress: this.props.navigation.navigate('Queue'),
            },
          ],
        );
      })
      .catch((error) => {
        if (error.code === 'auth/email-already-in-use') {
          Alert.alert(
            'Failed',
            'The email address provided is already in use',
            [
              {
                Text: 'Okay',
              },
            ],
          );
        }

        if (error.code === 'auth/invalid-email') {
          Alert.alert('Failed', 'The email address provided is invalid', [
            {
              Text: 'Okay',
            },
          ]);
        }
      });
  };
  render() {
    return (
      <ScrollView>
        <View style={signupStyle.primaryView}>
          <Text style={signupStyle.introText}>
            Create your account by filling in your details below
          </Text>
          <TextInput
            style={signupStyle.input}
            placeholder={'Username'}
            underlineColorAndroid="transparent"
            onChangeText={(text) => {
              this.setState({username: text});
            }}
          />
          {/* <TextInput
            style={signupStyle.input}
            placeholder={'Phone Number'}
            underlineColorAndroid="transparent"
            onChangeText={(text) => {
              this.setState({phone: text});
            }}
          /> */}
          <TextInput
            style={signupStyle.input}
            keyboardType="email-address"
            autoCapitalize="none"
            onChangeText={(text) => {
              this.setState({typedEmail: text});
            }}
            placeholder={'Enter email address'}
            underlineColorAndroid="transparent"
          />
          <TextInput
            style={signupStyle.input}
            placeholder={'Password'}
            underlineColorAndroid="transparent"
            secureTextEntry={true}
            onChangeText={(text) => {
              this.setState({typedPassword: text});
            }}
          />
          <TouchableOpacity
            style={signupStyle.submitBtn}
            onPress={this.onSignup}>
            <Text style={signupStyle.btnText}>SIGN UP</Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity style={signupStyle.secondaryBtn}>
          <Text style={signupStyle.loginText}>LOGIN</Text>
        </TouchableOpacity>
      </ScrollView>
    );
  }
}

const signupStyle = StyleSheet.create({
  primaryView: {
    flex: 1,
    width: WIDTH,
    padding: 30,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  input: {
    width: WIDTH - 80,
    height: 40,
    borderRadius: 7,
    paddingLeft: 45,
    backgroundColor: '#fff',
    marginTop: 20,
    color: 'black',
    marginHorizontal: 25,
  },
  submitBtn: {
    backgroundColor: '#FF5722',
    width: WIDTH - 200,
    height: 45,
    borderRadius: 0,
    marginTop: 40,
    color: 'white',
    marginHorizontal: 25,
    alignItems: 'center',
    alignSelf: 'center',
    backfaceVisibility: 'hidden',
  },
  secondaryBtn: {
    width: WIDTH - 100,
    height: 45,
    borderRadius: 0,
    marginTop: 40,
    color: 'white',
    marginHorizontal: 25,
    alignItems: 'center',
    alignSelf: 'center',
    backfaceVisibility: 'hidden',
  },
  btnText: {
    fontWeight: '500',
    fontSize: 17,
    top: 10,
    left: 0,
    right: 10,
    fontFamily: 'notoserif',
    color: '#FFFFFF',
  },
  loginText: {
    fontWeight: '500',
    fontSize: 17,
    top: 10,
    fontFamily: 'notoserif',
    color: 'green',
    position: 'absolute',
  },
  introText: {
    fontWeight: '600',
    fontSize: 15,
    margin: 20,
    fontFamily: 'notoserif',
  },
});

export default Signup;

import React, {Component} from 'react';
import {
  Text,
  ScrollView,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
  Picker,
  View,
  ActivityIndicator,
  Alert,
} from 'react-native';
// import DatePicker from 'react-native-date-picker';
import Icon from 'react-native-vector-icons/Fontisto';

// state management
import {connect} from 'react-redux';
import {saveQueueStates} from '/home/javier/final_Project/queueingSystem/QueueingSystem/app_modules/Actions/saveQueueStateActions.js';

// uuid generating
import uniqueId from 'react-native-unique-id';

const {width: WIDTH} = Dimensions.get('window');

class Queue extends Component {
  constructor() {
    super();
    this.state = {
      FromLocation: '',
      ToLocation: '',
      seats: '',
      remainingSeats: '',
      activeTicket: false,
      availableBuses: [
        {
          plates: 'KBZ 841K',
          capacity: 54,
          route: 'Waiyaki',
          stops: [
            {stop: 'Westlands'},
            {stop: 'Kangemi'},
            {stop: 'MountainView'},
            {stop: 'Kinoo'},
          ],
          active: false,
        },
        {
          plates: 'KAZ 090A',
          capacity: 14,
          route: 'Waiyaki',
          stops: [
            {stop: 'Westlands'},
            {stop: 'Kangemi'},
            {stop: 'MountainView'},
            {stop: 'Kinoo'},
          ],
          active: true,
        },
        {plates: 'KCD 724J', capacity: 54, route: 'Thika', active: false},
        {plates: 'KDF 351X', capacity: 54, route: 'Kibera', active: true},
        {
          plates: 'KBZ 841K',
          capacity: 54,
          route: 'Waiyaki',
          stops: [
            {stop: 'Westlands'},
            {stop: 'Kangemi'},
            {stop: 'MountainView'},
            {stop: 'Kinoo'},
          ],
          active: false,
        },
        {
          plates: 'KAZ 090A',
          capacity: 14,
          route: 'Waiyaki',
          stops: [
            {stop: 'Westlands'},
            {stop: 'Kangemi'},
            {stop: 'MountainView'},
            {stop: 'Kinoo'},
          ],
          active: true,
        },
        {plates: 'KCD 724J', capacity: 54, route: 'Thika', active: false},
        {plates: 'KDF 351X', capacity: 54, route: 'Kibera', active: true},
      ],
    };
  }

  Book = () => {
    let ToLocation = this.state.ToLocation;
    let FromLocation = this.state.FromLocation;
    let seats = this.state.seats;
    let activeTicket = this.state.activeTicket;
    var queueStates = {};
    // let remainingSeats = this.state.remainingSeats;

    queueStates.ToLocation = ToLocation;
    queueStates.FromLocation = FromLocation;
    queueStates.seats = seats;
    queueStates.activeTicket = activeTicket;

    if (FromLocation === '' || ToLocation === '' || seats === '') {
      Alert.alert(
        'Details Error',
        'You have to fill in all the fields to proceed',
        [
          {
            Text: 'Okay',
          },
        ],
      );
    } else {
      if (activeTicket === false) {
        this.setState({activeTicket: true});
        const activebuses = this.state.availableBuses.filter(
          (availableBuses) => availableBuses.active,
        );
        const waiyakiBuses = activebuses.filter(
          (availableBuses) => availableBuses.route === 'Waiyaki',
        );
        var goingBusCapacity = waiyakiBuses[0].capacity;
        if (
          ToLocation === 'Westlands' ||
          'Kangemi' ||
          'Kinoo' ||
          'MountainView'
        ) {
          if (waiyakiBuses !== null) {
            // console.log(goingBusCapacity);
            if (goingBusCapacity >= seats) {
              const remaining = goingBusCapacity - seats;
              goingBusCapacity = remaining;
              //give ticket by generating uuid
              uniqueId()
                .then((id) => console.log(id))
                .catch((error) => console.error(error));
              // Alert.alert('A ride has been found for you', uniqueId, [
              //   {
              //     Text: 'Proceed To Ticket',
              //   },
              // ]);
              // this.props.reduxSaveQueueStates(this.props);
              this.props.navigation.navigate('Ticket');
            } else {
              console.log('Capacity is less than seats');
            }
            // console.log(waiyakiBuses[0]);
          } else {
            console.log('No buses found');
          }
        } else {
          console.log('Your stop is not found');
        }
      } else {
        Alert.alert('Sorry', 'You already have an active ticket', [
          {
            Text: 'Okay',
          },
        ]);
      }
    }
  };

  render() {
    console.log(this.props);
    return (
      <ScrollView>
        <View style={homeStyle.primaryView}>
          <View>
            <Text style={homeStyle.descText}> BOOK YOUR RIDE </Text>
            <Picker
              style={homeStyle.picker}
              selectedValue={this.state.FromLocation}
              onValueChange={(FromLocation, itemIndex) =>
                this.setState({FromLocation: FromLocation})
              }>
              <Picker.item label="Select your pick up location" value="" />
              <Picker.item label="Nairobi" value="Nairobi" />
            </Picker>
            <Picker
              style={homeStyle.picker}
              selectedValue={this.state.ToLocation}
              onValueChange={(ToLocation) =>
                this.setState({ToLocation: ToLocation})
              }>
              <Picker.item label="Select your Destination" value="" />
              <Picker.item label="Westlands" value="Westlands" />
              <Picker.item label="Kangemi" value="Kangemi" />
              <Picker.item label="Mountain View" value="MountainView" />
              <Picker.item label="Kinoo" value="Kinoo" />
            </Picker>
            <Picker
              style={homeStyle.picker}
              selectedValue={this.state.seats}
              onValueChange={(seats) => this.setState({seats: seats})}>
              <Picker.item label="Select number of seats" value="" />
              <Picker.item label="1" value="1" />
              <Picker.item label="2" value="2" />
              <Picker.item label="3" value="3" />
            </Picker>
            {/* <TextInput
              style={homeStyle.picker}
              placeholder={'Number of Seats'}
              value={this.state.seats}
              onValueChange={(itemValue, itemIndex) =>
                this.setState({seats: itemValue})
              }
              underlineColorAndroid="transparent"
            /> */}
          </View>
          {/* <View style={homeStyle.scheduleView}>
            <Text style={homeStyle.scheduletext}>Schedule your trip</Text>
            <DatePicker
              style={homeStyle.DatePicker}
              date={this.state.date}
              // onDateChange={() => this.setState({date: new Date()})}
            />
          </View> */}
          <TouchableOpacity style={homeStyle.queueBtn} onPress={this.Book}>
            <Icon
              style={[
                {
                  color: 'white',
                  position: 'absolute',
                  left: 30,
                },
              ]}
              size={20}
              name={'bus-ticket'}
            />
            <Text style={homeStyle.btnText}>QUEUE</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const homeStyle = StyleSheet.create({
  primaryView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  scheduleView: {
    margin: 20,
    alignItems: 'center',
  },
  scheduletext: {
    top: 5,
    fontWeight: '600',
    fontSize: 15,
    fontFamily: 'notoserif',
    bottom: 5,
  },
  DatePicker: {
    margin: 10,
    fontSize: 15,
  },
  descText: {
    fontWeight: '700',
    fontSize: 17,
    fontFamily: 'notoserif',
    margin: 20,
    alignSelf: 'center',
  },
  picker: {
    width: WIDTH - 80,
    height: 40,
    borderRadius: 7,
    backgroundColor: '#fff',
    marginTop: 20,
    color: '#212121',
    marginHorizontal: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },
  queueBtn: {
    backgroundColor: '#FF5722',
    width: WIDTH - 200,
    height: 45,
    borderRadius: 0,
    marginTop: 40,
    color: 'white',
    flexDirection: 'row',
    justifyContent: 'center',
    marginHorizontal: 25,
    alignItems: 'center',
    alignSelf: 'center',
    backfaceVisibility: 'hidden',
  },
  btnText: {
    fontWeight: '500',
    fontSize: 17,
    fontFamily: 'notoserif',
    color: '#FFFFFF',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
});

const mapDispatchToProps = (dispatch) => {
  return {
    reduxSaveQueueStates: (queueStates) =>
      dispatch(saveQueueStates(queueStates)),
  };
};
// const mapStateToProps = (state) => ({reduxSaveQueueStates: state.queueStates});

export default connect(mapDispatchToProps, null)(Queue);

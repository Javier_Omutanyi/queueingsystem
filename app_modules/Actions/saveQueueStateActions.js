export const saveQueueStates = (queueStates) => ({
  type: 'SAVE_QUEUE_STATES',
  queueStates: {
    FromLocation: queueStates.FromLocation,
    ToLocation: queueStates.ToLocation,
    seats: queueStates.seats,
    remainingSeats: queueStates.remainingSeats,
    activeTicket: queueStates.activeTicket,
    availableBuses: queueStates.availableBuses,
  },
});

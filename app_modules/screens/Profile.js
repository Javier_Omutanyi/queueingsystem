import React, {Component} from 'react';
import {
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  ScrollView,
  View,
} from 'react-native';
import {CheckBox} from 'react-native-elements';

const {width: WIDTH} = Dimensions.get('window');

class Profile extends Component {
  state = {
    phone: '0700112233',
    checked: 'false',
  };
  render() {
    return (
      <ScrollView>
        <View style={profileStyles.primaryView}>
          <Text style={profileStyles.introText}>
            {' '}
            Change your phone number{' '}
          </Text>
          <TextInput
            style={profileStyles.input}
            placeholder={this.state.phone}
            underlineColorAndroid="transparent"
          />
          <Text style={profileStyles.introText}> Manage Your Notications </Text>
          <CheckBox
            center
            title="Stop SMS notifications"
            checkedIcon="dot-circle-o"
            uncheckedIcon="circle-o"
            checked={this.state.checked}
          />
        </View>
        <TouchableOpacity style={profileStyles.submitBtn}>
          <Text style={profileStyles.btnText}>SUBMIT</Text>
        </TouchableOpacity>
      </ScrollView>
    );
  }
}

const profileStyles = StyleSheet.create({
  primaryView: {
    flex: 1,
    width: WIDTH,
    padding: 30,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  input: {
    width: WIDTH - 80,
    height: 40,
    borderRadius: 7,
    paddingLeft: 45,
    backgroundColor: '#fff',
    marginTop: 0,
    color: 'black',
    marginHorizontal: 25,
  },
  submitBtn: {
    backgroundColor: '#FF5722',
    width: WIDTH - 200,
    height: 45,
    borderRadius: 0,
    marginTop: 40,
    color: 'white',
    marginHorizontal: 25,
    alignItems: 'center',
    alignSelf: 'center',
    backfaceVisibility: 'hidden',
  },
  btnText: {
    fontWeight: '500',
    fontSize: 17,
    top: 10,
    left: 0,
    right: 10,
    fontFamily: 'notoserif',
    color: '#FFFFFF',
  },
  introText: {
    fontWeight: '600',
    fontSize: 17,
    margin: 20,
    fontFamily: 'notoserif',
  },
});

export default Profile;

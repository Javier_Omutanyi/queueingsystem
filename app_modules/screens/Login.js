import React, {Component} from 'react';
import {
  Text,
  Dimensions,
  ScrollView,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  View,
  Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

// firebase authentication import
import firebase from '@react-native-firebase/app';
import auth from '@react-native-firebase/auth';

const {width: WIDTH} = Dimensions.get('window');

class Login extends Component {
  constructor(props) {
    super(props);
    this.unsubscriber = null;
    this.state = {
      isAuthenticated: false,
      username: '',
      // phone: '',
      typedEmail: '',
      typedPassword: '',
      user: null,
    };
  }

  login = () => {
    firebase
      .auth()
      .signInWithEmailAndPassword(
        this.state.typedEmail,
        this.state.typedPassword,
      )
      .then(() => {
        Alert.alert('Successfull', 'Login successfull', [
          {
            Text: 'Okay',
            onPress: this.props.navigation.navigate('Queue'),
          },
        ]);
      })
      .catch((error) => {
        Alert.alert(
          'Failed',
          'The email/password provided does not have an account',
          [
            {
              Text: 'Okay',
            },
          ],
        );
      });
  };
  render() {
    return (
      <ScrollView>
        <View style={loginStyle.primaryView}>
          <Text style={loginStyle.titleText}>QUEUEING SYSTEM</Text>
          <Text style={loginStyle.introText}>Login to book your ride</Text>
          <TextInput
            style={loginStyle.input}
            placeholder={'Email'}
            value={this.state.typedEmail}
            onChangeText={(text) => this.setState({typedEmail: text})}
            underlineColorAndroid="transparent"
          />
          <TextInput
            style={loginStyle.input}
            placeholder={'Password'}
            secureTextEntry={true}
            value={this.state.typedPassword}
            onChangeText={(text) => this.setState({typedPassword: text})}
            underlineColorAndroid="transparent"
          />
          <TouchableOpacity
            style={loginStyle.submitBtn}
            onPress={() => this.login()}>
            <Icon
              style={[
                {
                  color: 'white',
                  position: 'absolute',
                  right: 10,
                },
              ]}
              size={20}
              name={'login'}
            />
            <Text style={loginStyle.btnText}>LOGIN</Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          style={loginStyle.secondaryBtn}
          onPress={() => this.props.navigation.navigate('Signup')}>
          <Text style={loginStyle.createText}>CREATE ACCOUNT</Text>
        </TouchableOpacity>
      </ScrollView>
    );
  }
}

const loginStyle = StyleSheet.create({
  primaryView: {
    flex: 1,
    width: WIDTH,
    padding: 30,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  input: {
    width: WIDTH - 80,
    height: 40,
    borderRadius: 7,
    paddingLeft: 45,
    backgroundColor: '#fff',
    marginTop: 20,
    color: 'black',
    marginHorizontal: 25,
  },
  submitBtn: {
    backgroundColor: '#FF5722',
    width: WIDTH - 200,
    height: 45,
    borderRadius: 0,
    marginTop: 40,
    alignItems: 'center',
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    color: 'white',
    marginHorizontal: 25,
    backfaceVisibility: 'hidden',
  },
  secondaryBtn: {
    width: WIDTH - 100,
    height: 45,
    borderRadius: 0,
    marginTop: 40,
    color: 'white',
    marginHorizontal: 25,
    alignItems: 'center',
    alignSelf: 'center',
    backfaceVisibility: 'hidden',
  },
  btnText: {
    fontWeight: '500',
    fontSize: 17,
    top: 10,
    fontFamily: 'notoserif',
    color: '#FFFFFF',
    position: 'absolute',
  },
  createText: {
    fontWeight: '500',
    fontSize: 17,
    top: 10,
    fontFamily: 'notoserif',
    color: 'green',
    position: 'absolute',
  },
  titleText: {
    fontWeight: '700',
    fontSize: 18,
    margin: 20,
    fontFamily: 'notoserif',
  },
  introText: {
    fontWeight: '600',
    fontSize: 15,
    margin: 20,
    fontFamily: 'notoserif',
  },
});

export default Login;

import React, {Component} from 'react';
import {
  Text,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  Dimensions,
  View,
} from 'react-native';
import {connect} from 'react-redux';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const {width: WIDTH} = Dimensions.get('window');

class Ticket extends Component {
  state = {
    ticketNumber: 345,
    busNumber: 'KCZ 309X',
    waitingTime: '30 MIN',
    seats: 4,
  };
  render() {
    return (
      <ScrollView style={ticketStyle.body}>
        <View style={ticketStyle.primaryView}>
          <Text style={ticketStyle.ticketText}>
            {' '}
            Ticket Number :{' '}
            <Text style={ticketStyle.keyText}> {this.state.ticketNumber} </Text>
            For {this.props.queueStates.seats} seats
          </Text>
          <Text style={ticketStyle.ticketText}>
            {' '}
            You have been scheduled to travel with bus number :
            <Text style={ticketStyle.keyText}>{this.state.busNumber}</Text>
          </Text>
          <Text style={ticketStyle.ticketText}>
            {' '}
            Your estimated waiting time is :
            <Text style={ticketStyle.keyText}>{this.state.waitingTime}</Text>
          </Text>
        </View>
        <View style={ticketStyle.secondaryView}>
          <TouchableOpacity
            style={ticketStyle.menuBtn}
            onPress={() => this.props.navigation.navigate('Track')}>
            <Icon
              style={[
                {
                  position: 'absolute',
                  left: 30,
                },
              ]}
              size={20}
              name={'track-light'}
            />
            <Text style={ticketStyle.btnText}>Track Bus</Text>
          </TouchableOpacity>
          {/* <TouchableOpacity
            style={ticketStyle.menuBtn}
            onPress={() => this.props.navigation.navigate('Manage')}>
            <Text style={ticketStyle.btnText}>Ticket Options</Text>
          </TouchableOpacity> */}
          <TouchableOpacity
            style={ticketStyle.menuBtn}
            onPress={() => this.props.navigation.navigate('Notifications')}>
            <Icon
              style={[
                {
                  position: 'absolute',
                  left: 10,
                },
              ]}
              size={20}
              name={'settings-outline'}
            />
            <Text style={ticketStyle.btnText}>Manage Notifications</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={ticketStyle.menuBtn}
            onPress={() => this.props.navigation.navigate('Profile')}>
            <Icon
              style={[
                {
                  position: 'absolute',
                  left: 30,
                },
              ]}
              size={20}
              name={'account-circle-outline'}
            />
            <Text style={ticketStyle.btnText}>Profile</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={ticketStyle.menuBtn}
            onPress={() => this.props.navigation.navigate('Records')}>
            <Icon
              style={[
                {
                  position: 'absolute',
                  left: 30,
                },
              ]}
              size={20}
              name={'view-list'}
            />
            <Text style={ticketStyle.btnText}>View Records</Text>
          </TouchableOpacity>
          <TouchableOpacity style={ticketStyle.cancelBtn}>
            <Icon
              style={[
                {
                  position: 'absolute',
                  left: 30,
                  color: 'white',
                },
              ]}
              size={20}
              name={'cancel'}
            />
            <Text style={ticketStyle.cancelText}>CANCEL TICKET</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const ticketStyle = StyleSheet.create({
  primaryView: {
    flex: 1,
    height: 200,
    width: WIDTH,
    backgroundColor: '#212121',
    padding: 30,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  ticketText: {
    color: 'white',
    fontWeight: '400',
    fontSize: 16,
    fontFamily: 'notoserif',
    bottom: 5,
  },
  keyText: {
    color: '#FF5722',
    fontWeight: '700',
    fontSize: 18,
    fontFamily: 'notoserif',
    bottom: 5,
  },
  secondaryView: {
    flex: 1,
    width: WIDTH,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  body: {
    backgroundColor: '#D7CCC8',
  },
  menuBtn: {
    width: WIDTH - 150,
    padding: 15,
    marginTop: 15,
    marginHorizontal: 25,
    alignItems: 'center',
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    backfaceVisibility: 'hidden',
    backgroundColor: 'white',
  },
  cancelBtn: {
    width: WIDTH - 100,
    padding: 15,
    marginTop: 15,
    marginHorizontal: 25,
    alignItems: 'center',
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    backfaceVisibility: 'hidden',
    backgroundColor: 'red',
  },
  btnText: {
    fontWeight: '600',
    fontSize: 17,
    left: 0,
    right: 10,
    fontFamily: 'notoserif',
    color: '#212121',
  },
  cancelText: {
    fontWeight: '600',
    fontSize: 17,
    left: 0,
    right: 10,
    fontFamily: 'notoserif',
    color: 'white',
  },
});

const mapStateToProps = (state) => {
  return {
    queueStates: state.queueStatesReducer.queueStates,
  };
};

export default connect(mapStateToProps, null)(Ticket);

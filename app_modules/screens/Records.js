import React, {Component} from 'react';
import {Text, ScrollView, Image, StyleSheet, View} from 'react-native';
import {ListItem} from 'react-native-elements';

import Icon from 'react-native-vector-icons/Fontisto';

const list = [
  {
    ticket: 'SzdHy126z',
    date: '26/3/2019',
    time: '23:00',
    bus: 'KCZ 345H',
  },
  {
    ticket: 'SzdHy126z',
    date: '26/3/2019',
    time: '23:00',
    bus: 'KCZ 345H',
  },
];

class Records extends Component {
  render() {
    return (
      <ScrollView>
        <View>
          {list.map((item, i) => (
            <ListItem
              key={i}
              title={
                <View style={styles.subtitleView}>
                  <Text style={styles.ratingText}>Ticket Number :</Text>
                  <Text style={styles.ratingText}>{item.ticket}</Text>
                </View>
              }
              subtitle={
                <View style={styles.subtitleView}>
                  <Text style={styles.ratingText}>{item.date}</Text>
                  <Text style={styles.ratingText}>{item.time}</Text>
                  <Text style={styles.ratingText}>{item.bus}</Text>
                </View>
              }
              leftIcon={
                <Icon
                  style={[
                    {
                      position: 'absolute',
                      left: 30,
                      right: 20,
                    },
                  ]}
                  size={20}
                  name={'bus-ticket'}
                />
              }
              bottomDivider
            />
          ))}
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  subtitleView: {
    flexDirection: 'row',
    paddingLeft: 10,
    paddingTop: 5,
  },
  // ratingImage: {
  //   height: 19.21,
  //   width: 100,
  // },
  ratingText: {
    paddingLeft: 10,
    color: 'grey',
  },
});

export default Records;

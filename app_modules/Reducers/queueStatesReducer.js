const initialState = {
  queueStates: {
    FromLocation: '',
    ToLocation: '',
    seats: '',
    remainingSeats: '',
    activeTicket: false,
    availableBuses: '',
  },
};
const queueStatesReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SAVE_QUEUE_STATES': {
      return {
        ...state,
        queueStates: action.queueStates,
      };
    }
    default: {
      return state;
    }
  }
};
export default queueStatesReducer;
